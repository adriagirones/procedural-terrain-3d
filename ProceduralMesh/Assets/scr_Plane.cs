﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Plane : MonoBehaviour
{
    Rigidbody rb;
    public GameObject plain;
    Transform tr;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        tr = plain.GetComponent<Transform>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.AddForce(0, 9.81f, 0);

        if (Input.GetKey(KeyCode.A))
        {
            rb.AddForce(-10f, 0, 0);
            tr.Rotate(0f, -5f * Time.fixedDeltaTime, 10f * Time.fixedDeltaTime, Space.World);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rb.AddForce(10f, 0, 0);
            tr.Rotate(0f, 5f * Time.fixedDeltaTime, -10f * Time.fixedDeltaTime, Space.World);
        } 

        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(0, 10f, 0);
            tr.Rotate(-10 * Time.fixedDeltaTime, 0, 0);
        }  
        if (Input.GetKey(KeyCode.S))
        {
            rb.AddForce(0, -10f, 0);
            tr.Rotate(10 * Time.fixedDeltaTime, 0, 0);
        }    

        if (rb.velocity.magnitude > 15f)
            rb.velocity = rb.velocity.normalized * 15f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ring")
            print("ring");
    }

}
