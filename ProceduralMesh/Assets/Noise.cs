﻿using UnityEngine;
using System.Collections;

public static class Noise {

	public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float lacunarity, float persistence, Vector2 offset) {
		float[,] noiseMap = new float[mapWidth,mapHeight];

		System.Random random = new System.Random (seed);

        Vector2[] offSetOctaves = new Vector2[octaves];

        for(int i = 0; i < octaves; i++)
        {
            Vector2 offSetReal = new Vector2();
            offSetReal.x = random.Next(-100000, 100000) + offset.x;
            offSetReal.y = random.Next(-100000, 100000) + offset.y;
            offSetOctaves[i] = offSetReal;
        }
        

        if (scale <= 0) {
			scale = 0.0001f;
		}

		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;


		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
		
				float amplitude = 1;
				float frequency = 1;
				float noiseHeight = 0;

                for (int i = 0; i < octaves; i++) {

                    float sampleX = (x - halfWidth) / scale * frequency + offSetOctaves[i].x;
                    float sampleY = (y - halfHeight) / scale * frequency + offSetOctaves[i].y;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistence;
                    frequency *= lacunarity;

                }

				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				} else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}
				noiseMap [x, y] = noiseHeight;
			}
		}

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				noiseMap [x, y] = Mathf.InverseLerp (minNoiseHeight, maxNoiseHeight, noiseMap [x, y]);
			}
		}

       // Debug.Log(noiseMap[0, 0]);

		return noiseMap;
	}

	public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, float freq, Vector2 offset)
	{
		float[,] noiseMap = new float[mapWidth, mapHeight];

		//System.Random random = new System.Random(seed);

		Vector2 offSetReal = new Vector2();
		//offSetReal.x = random.Next(-100000, 100000) + offset.x;
		//offSetReal.y = random.Next(-100000, 100000) + offset.y;

		offSetReal.x = seed + offset.x;
		offSetReal.y = seed + offset.y;

		if (scale <= 0)
		{
			scale = 0.0001f;
		}

		float maxNoiseHeight = float.MinValue;
		float minNoiseHeight = float.MaxValue;

		float halfWidth = mapWidth / 2f;
		float halfHeight = mapHeight / 2f;


		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{

				float amplitude = 1;
				float noiseHeight = 0;

				float sampleX = (x - halfWidth) / freq + offSetReal.x * (scale / freq);
				float sampleY = (y - halfHeight) / freq + offSetReal.y;

				float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
				noiseHeight += perlinValue * amplitude;

				if (noiseHeight > maxNoiseHeight)
				{
					maxNoiseHeight = noiseHeight;
				}
				else if (noiseHeight < minNoiseHeight)
				{
					minNoiseHeight = noiseHeight;
				}
				noiseMap[x, y] = noiseHeight;
			}
		}

		for (int y = 0; y < mapHeight; y++)
		{
			for (int x = 0; x < mapWidth; x++)
			{
				noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
			}
		}

		return noiseMap;
	}

}
