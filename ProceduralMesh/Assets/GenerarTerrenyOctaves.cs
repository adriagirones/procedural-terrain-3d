﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarTerrenyOctaves : MonoBehaviour
{
    public int ampladax = 256;
    public int llargadaz = 256;
    public int amplitud = 60;
    private float noiseScale = 100;

    [Range(1, 4)]
    public int octaves;
    public float lacunarity;
    [Range(0, 0.2f)]
    public float persistence;

    public int seed;
    public Vector2 offset;

    public bool autoUpdate;

    List<GameObject> listSnowPine;
    List<GameObject> listPine;
    List<GameObject> listGrass;

    public Pool poolSnowPine;
    public Pool poolPine;
    public Pool grass;

    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startngHeight;
    }

    Terrain terrain;

    public SplatHeights[] splatHeights;

    // Start is called before the first frame update
    void Start()
    {
        listSnowPine = new List<GameObject>();
        listPine = new List<GameObject>();
        listGrass = new List<GameObject>();

        terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerarProceduralment(terrain.terrainData);
        terrain.terrainData.SetAlphamaps(0, 0, GenerarColor(terrain.terrainData));
        StartCoroutine(UpdateTerrain());
        RenderSettings.fog = true;
    }

    IEnumerator UpdateTerrain()
    {
        while (true)
        {
            foreach (GameObject go in listSnowPine)
                poolSnowPine.ReturnObjToPool(go);

            foreach (GameObject go in listGrass)
                grass.ReturnObjToPool(go);

            foreach (GameObject go in listPine)
                poolPine.ReturnObjToPool(go);

            listSnowPine.Clear();
            listGrass.Clear();
            listPine.Clear();

            offset.x += 0.01f;
            terrain.terrainData = GenerarProceduralment(terrain.terrainData);
            terrain.terrainData.SetAlphamaps(0, 0, GenerarColor(terrain.terrainData));
            yield return new WaitForSecondsRealtime(0.03f);
        }
    }

    private TerrainData GenerarProceduralment(TerrainData td)
    {
        td.heightmapResolution = ampladax + 1;
        td.size = new Vector3(ampladax, amplitud, llargadaz);
        td.SetHeights(0, 0, GenerarAltures());
        return td;
    }

    public int seedTree;
    public int seedGrass;
    public float amplitudTree;
    public float freqTree;
    public float freqGrass;

    private float[,] GenerarAltures()
    {
        float[,] noiseMap = Noise.GenerateNoiseMap(ampladax, llargadaz, seed, noiseScale, octaves, lacunarity, persistence, offset);
        float[,] probTree = Noise.GenerateNoiseMap(ampladax, llargadaz, seedTree, noiseScale, freqTree, offset);
        float[,] probGrass = Noise.GenerateNoiseMap(ampladax, llargadaz, seedGrass, noiseScale, freqGrass, offset);

        for (int x = 0; x < ampladax; x++)
        {
            for (int z = 0; z < llargadaz; z++)
            {  
                if (probTree[x,z] > 0.9f && noiseMap[x, z] * amplitudTree > 13)
                {
                    GameObject newTree;
                    if (noiseMap[x, z] * amplitudTree < 45)
                    {
                        newTree = poolPine.GetObjFromPool();
                        newTree.transform.position = new Vector3(z, noiseMap[x, z] * amplitudTree, x);
                        listPine.Add(newTree.gameObject);
                    }
                    else
                    {
                        newTree = poolSnowPine.GetObjFromPool();
                        newTree.transform.position = new Vector3(z, noiseMap[x, z] * amplitudTree, x);
                        listSnowPine.Add(newTree.gameObject);    
                    }             
                }
                else if (probGrass[x, z] > 0.75f && (noiseMap[x, z] * amplitudTree > 13) && (noiseMap[x, z] * amplitudTree < 25))
                {
                    GameObject newGrass = grass.GetObjFromPool();
                    newGrass.transform.position = new Vector3(z, noiseMap[x, z] * amplitudTree, x);
                    listGrass.Add(newGrass.gameObject);
                }
            }
        }

        return noiseMap;
    }

    private float[,,] GenerarColor(TerrainData terrainData)
    {
        terrainData.alphamapResolution = ampladax + 1;
        float[,,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float terrainHeight = terrainData.GetHeight(y, x);

                float[] splat = new float[splatHeights.Length];

                for (int i = 0; i < splatHeights.Length; i++)
                {
                    if (i == splatHeights.Length - 1 && terrainHeight >= splatHeights[i].startngHeight)
                        splat[i] = 1;
                    else if (terrainHeight >= splatHeights[i].startngHeight && terrainHeight <= splatHeights[i + 1].startngHeight)
                        splat[i] = 1;
                }

                for (int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }

            }
        }
        return splatmapData;
    }
}
