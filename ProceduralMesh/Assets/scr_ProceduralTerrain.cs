﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ProceduralTerrain : MonoBehaviour
{

    public int amplitud = 20;

    private int ampladax = 256;
    private int llargadaz = 256;
    public float freq = 20;

    public float seed = 2313223f;

    Terrain terrain;

    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startngHeight;
    }

    public SplatHeights[] splatHeights;

    private void Start()
    {
        terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerarTerrain(terrain.terrainData);
        terrain.terrainData.SetAlphamaps(0, 0, GenerarColor(terrain.terrainData));
    }

    private float[,,] GenerarColor(TerrainData terrainData)
    {
        terrainData.alphamapResolution = ampladax + 1;
        float[,,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
        for(int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for(int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float terrainHeight = terrainData.GetHeight(y, x);

                float[] splat = new float[splatHeights.Length];
                
                for(int i = 0; i< splatHeights.Length; i++)
                {
                    if (i == splatHeights.Length-1 && terrainHeight >= splatHeights[i].startngHeight)
                        splat[i] = 1;
                    else if (terrainHeight >= splatHeights[i].startngHeight && terrainHeight <= splatHeights[i+1].startngHeight)
                        splat[i] = 1;
                }

                for(int j = 0; j < splatHeights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }

            }
        }
        return splatmapData;
    }

    private void Update()
    {
        terrain.terrainData = GenerarTerrain(terrain.terrainData);
        terrain.terrainData.SetAlphamaps(0, 0, GenerarColor(terrain.terrainData));
    }

    private TerrainData GenerarTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = ampladax + 1;
        terrainData.size = new Vector3(ampladax, amplitud, llargadaz);

        terrainData.SetHeights(0, 0, GenerarAltura());

        return terrainData;

    }

    public float comunisme = 50f;
    public GameObject joan;

    private float[,] GenerarAltura()
    {
        float[,] altures = new float[ampladax, llargadaz];
        for (int x = 0; x < ampladax; x++)
        {
            for (int z = 0; z < llargadaz; z++)
            {
               // float probJoan = Mathf.PerlinNoise((x + comunisme) / freq, (z + comunisme) / freq);

                float y = Mathf.PerlinNoise(x / freq + seed, z / freq + seed);

                altures[x, z] = y;

               /* if (probJoan > 0.9f)
                {
                    GameObject newJoan = Instantiate(joan);
                    newJoan.transform.position = new Vector3(z, y * amplitud, x);
                }*/
            }
        }
        return altures;
    }
}
